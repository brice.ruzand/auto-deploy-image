FROM docker:stable

ARG HELM_VERSION
ARG KUBERNETES_VERSION

COPY src/ build/

# Install Dependencies
RUN apk add -U openssl curl tar gzip bash ca-certificates git \
  && curl -sS "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx \
  && mv linux-amd64/helm   /usr/local/bin/ \
  && mv linux-amd64/tiller /usr/local/bin/ \
  && curl -sSL -o /usr/local/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl" \
  && chmod +x /usr/local/bin/kubectl
  
RUN ln -s /build/bin/* /usr/local/bin/
